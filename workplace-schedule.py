import openpyxl
from datetime import date
from babel.dates import format_date
from dateutil.rrule import rrule, DAILY

wb = openpyxl.load_workbook('building-plan.xlsx')
# get the only worksheet's name
original_sheet = wb.sheetnames[0]

start_date = date(2021, 3, 1)
end_date = date(2021, 3, 31)

for dt in rrule(DAILY, dtstart=start_date, until=end_date):
    # create a new name for each new worksheet.
    # format: "J 10-septiembre-2020"
    # See: http://babel.pocoo.org/en/latest/dates.html
    nombre_nueva_hoja = format_date(dt,"EEEEE dd-MMMM-yyyy", locale='es_AR')
    # copy the first worksheet
    target = wb.copy_worksheet(wb[original_sheet])
    # change name of new worksheet
    target.title = nombre_nueva_hoja

# save the new Excel file, ready for Google Docs, Collabora or OnlyOffice
wb.save('shcedule-plan.xlsx')
