# workplace-schedule

Python script to turn a one-sheet Excel file into a multi-sheet Excel file, with one sheet per day for some period of time.

## Rationale

After the COVID-19 quarantine, at my work we needed a way to organize who went which day to different offices and laboratories. The simple and dirty solution was to have an Excel file hosted in Google Docs, with a rough plan of the building in different sheets for each day. But nobody wants to copy and paste Excel sheets by hand, so Python came to the rescue. 

## Dependencies

- openpyxl
- datetime
- babel.dates
- dateutil.rrule
